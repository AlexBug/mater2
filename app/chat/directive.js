/**
 * Author: Alex Guo (chessnut@outlook.com).
 *
 * Directive for chatbox.
 */

(function(){

function calcChatBoxRightOffset(chat_num) {
  var widthOffset = 20;
  var widthInc = 287;
  var width = chat_num * widthInc + widthOffset;
  return width;
}

angular.module('mater2.directives').directive('chatbox', function(){
  return {
    restrict: 'EA',
    scope: {
      chatNum: '@',
      interlocutorName: '@',
      messages: '=messages',
      maximized: '=',
      onChatCloseRef: '&onChatClose',
      onSendMsgRef: '&onSendMsg',
      new_msg: '=',
    },
    templateUrl: 'app/chat/chatbox.html',
    link: function($scope, element, attrs) {
      element.addClass("chatbox");
      element.css("position", "fixed");
      element.css("bottom", "0px");
      element.css("right", calcChatBoxRightOffset($scope.chatNum));
      element.css("width", "280px");
      element.css("display", "block");
      element.css("z-index", "999999");

      if (!$scope.maximized) { $scope.maximized = true; }
      if (!$scope.new_msg) { $scope.new_msg = ""; }
      $scope.onChatClose = $scope.onChatCloseRef();
      $scope.onSendMsg = function(interlocutor_name, new_msg) {
        $scope.new_msg = "";
        $scope.onSendMsgRef()(interlocutor_name, new_msg);
      }
    },
  };
});

})();
