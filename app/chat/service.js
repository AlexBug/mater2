/**
 * Service for chatting to people
 */

(function(){

angular.module('mater2.services').service('ChatService',
['IcsService', 'ProfileService', 'ImcService',
function(IcsService, ProfileService, ImcService) {
  var _chatGroups = [];
  var _hiddenChats = [];

  function findOrCreateChatGroup(name) {
    for (var i = 0; i != _chatGroups.length; ++i){
      var chatGroup = _chatGroups[i];
      if (chatGroup.talkingTo == name){
        return chatGroup;
      }
    }
    var chatGroup = {
      heading: name,
      talkingTo: name,
      currentText: '',
      active: true,
      messages : []
    };
    _chatGroups.push(chatGroup);
    return chatGroup;
  }

  function ToldSomeoneMessage(response) {
    var regexp = /\(told ([A-Za-z]*)(, who is playing\)|\))/;
    var m = regexp.exec(response.data);
    var name = m[1];
    var chatGroup = findOrCreateChatGroup(name);
    chatGroup.messages.push({
      name: ProfileService.GetUsername(),
      msg: chatGroup.sentText,
      time: moment().format('h:mm A'),
    });
    chatGroup.sentText = '';
  }

  function WasToldMessage(response) {
    var strs = response.data.split("\n");
    for (var i = 0; i < strs.length; ++i) {
      var regexp1 = /([A-Za-z]+)(\(.*?\))* tells you: (.*)/m;
      var regexp2 = /([A-Za-z]+)(\(.*?\))* \(your partner\) tells you: (.*)/m;
      var m = regexp1.exec(strs[i]);
      if (m === null || m === undefined) { m = regexp2.exec(strs[i]); }
      if (m === null || m === undefined) { continue; }
      var sender = m[1];
      var msg = m[3];
      var chatGroup = findOrCreateChatGroup(sender);
      chatGroup.messages.push({
        name: sender,
        msg: msg,
        time: moment().format('h:mm A'),
      });
    }
  }

  function SendMsg(args) {
    var name = args.tellTo;
    var msg = args.msg;
    var cmd = 'tell ' + name + ' ' + msg;
    findOrCreateChatGroup(name).sentText = msg;
    IcsService.SendLine(cmd);
  }

  function HideChat(args) {
    args.heading;
  }

  // Listen for session-related events.
  IcsService.AddListener("told someone message", ToldSomeoneMessage);
  IcsService.AddListener("was told message", WasToldMessage);

  ImcService.addHandler('open chat group', function(data) {
    findOrCreateChatGroup(data.name);
  });
  return {
    SendMsg: SendMsg,
    HideChat: HideChat,
    chatGroups: _chatGroups,
  }
}]);

})();
