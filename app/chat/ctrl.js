(function(){

angular.module('mater2.controllers').controller('ChatController',
  ['$scope', 'ChatService', 'LocalizationService', controller]);
function controller($scope, ChatService, LocalizationService) {
  $scope.locale = LocalizationService.Locale;
  $scope.chatGroups = ChatService.chatGroups;

  $scope.sendMsg = function(interlocutor_name, new_msg) {
    ChatService.SendMsg({
      tellTo: interlocutor_name,
      msg: new_msg,
    });
  }

  $scope.onChatClose = function(interlocutorName) {
    for (var j = 0; j < $scope.chatGroups.length; j++) {
      if (interlocutorName === $scope.chatGroups[j].talkingTo) {
        $scope.chatGroups.splice(j, 1);
        break;
      }
    }
  }
}

})();
