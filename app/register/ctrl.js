/**
 * Author: Anthony Guo (anthony.guo@some.ox.ac.uk)
 * Purpose: A controller for logging in to the server
 */
(function(){

angular.module('mater2.controllers').controller('RegisterCtrl',
  ['$scope', 'LocalizationService', '$mdDialog', 'RegisterService',
   'LoginService', controller])
function controller($scope, LocalizationService, $mdDialog, RegisterService,
                    LoginService) {
  $scope.locale = LocalizationService.Locale;

  $scope.input = {
    username: '',
    password: '',
    email: '',
  };

  function onRegistrationSuccess() {
    LoginService.Login($scope.input, onLoginSuccess);
  }

  function onLoginSuccess() {
    $mdDialog.hide({ status: 'success' });
  }

  $scope.AttemptRegister = function() {
    RegisterService.Register($scope.input, onRegistrationSuccess);
  }
}

})();
