/**
 * Service for logins, instead of controller.
 * If controller, then multiple invocations would
 * disrupt invariants. For instance, multiple
 * listeners might get added.
 */

(function(){

angular.module('mater2.services').service('RegisterService',
  ['BackendAdapters', 'toaster',
function(BackendAdapters, toaster) {
  var _callback = null;

  function validateEmail(email) {
      var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);
  }

  function checkInput(data) {
    if (data.username.length < 3) {
// TODO: fix locale issue here.
      toaster.pop("error", "Username must be at least 3 characters.");
      return false;
    }
    if (data.username.length >= 20) { return false; }
    if (data.password.length <= 0) { return false; }
    if (!validateEmail(data.email)) { return false; }
    return true;
  }

  function Register(data, callback) {
    if (!checkInput(data)) { return; }
    _callback = callback;
    BackendAdapters.Do("register", data);
  }

  function onError(data) {
    toaster.pop("error", data.msg);
  }

  function onSuccess(data) {
    toaster.pop("success", "You're registered! One moment please...");
    _callback();
  }

  BackendAdapters.AddListener(onError, "register", "error");
  BackendAdapters.AddListener(onSuccess, "register", "success");

  return {
    "Register": Register,
  };
}]);

})();
