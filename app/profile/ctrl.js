/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * Controller for displaying information about the current user
 * This will include the current partner, rating, etc.
 */

(function(){
    angular.module('mater2.controllers').controller('ProfileCtrl',
        ['$scope', 'ProfileService', controller]);
    function controller($scope, ProfileService){
        $scope.profile_data = ProfileService.profile_data;
    }
})();
