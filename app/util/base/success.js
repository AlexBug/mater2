/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * A success controller 
 */
(function(){
    angular.module('mater2.controllers').controller('SuccessCtrl',
        ['$scope',  'LocalizationService',  controller])
    function controller($scope, LocalizationService ){
        $scope.locale = LocalizationService.Locale;
    }
})();
