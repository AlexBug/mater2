/**
 * Author: Anthony Guo (anthony.guo@some.ox.ac.uk)
 *
 * Purpose: Service that provides mappings to different languages.
 *          Currently supports:
 *          - English
 *          - Russian
 */

(function() {

angular.module('mater2.services').service('LocalizationService',
  ['ConfigService' , '$http', '$rootScope', service]);
function service(ConfigService, $http, $rootScope) {
  function GetLocale(lang) {
    return $http.get('app/util/locales/' + lang + '.json');
  }

  function UpdateLocale() {
    var promise = null;
    var lang = ConfigService.GetLanguage();
    if (lang == 'english' || lang == 'russian' || lang == 'chinese') {
      promise = GetLocale(lang);
    }
    promise.success(function(locale) {
      for (var word in locale){
        locale_in_use[word] = locale[word];
      }
    });
  }

  $rootScope.$on('$locationChangeSuccess', function() {
    UpdateLocale();
  });

  var locale_in_use = {};
  UpdateLocale();
  return {
    Locale: locale_in_use
  };
}

})();
