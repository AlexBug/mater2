/* Anthony Guo (anthony.guo@some.ox.ac.uk)
 * Service that parses the URL and provides configuration options
 */

(function(){

angular.module('mater2.services').service('ConfigService', ['$location', service]);
  function service($location){ 
    function InDevMode() {
      var searchObj = $location.search();
      return searchObj.dev;
    }

    function GetLanguage(){
      var searchObj = $location.search();
      if (searchObj.lang){
        return searchObj.lang.toLowerCase();
      } else {
        return 'english';
      }
    }

    return {
      InDevMode: InDevMode,

      // Returns the current language based on the URL configuration
      // Gets argumnents from the hash of the URL
      //   e.g. http://..../#?lang={SOME_LANGUAGE}
      // Defaults to English if no language is specified
      GetLanguage: GetLanguage
    }
}

})();
