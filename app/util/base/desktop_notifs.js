/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 * Purpose of this service is to deal with desktop notifications
 */

(function(){

angular.module('mater2.services').service('DesktopNotifs', 
  ['ImcService', service]);
if ("Notification" in window) {
  // if notifications are avail, immediately request
  Notification.requestPermission()
}
var isActive = true;

window.onfocus = function () { 
  isActive = true; 
}; 

window.onblur = function () { 
  isActive = false; 
}; 

function service(ImcService){
  var current_notif = null;
  function FireNotification(notification) {
    if (!isActive){
      // Try to send a desktop notif
      if ("Notification" in window) {
        var opts = {
          message: notification.message,
          body: notification.message
        };
        var notification = new Notification(notification.title, opts);
        // To make sure that only at maximum, one notification is open
        if (current_notif){
          current_notif.close();
        }
        current_notif = notification;
        setTimeout(function(){
          notification.close();
        }, 5 * 1000);
      }
      // Try to trigger a sound
      var snd = new Audio("assets/sound/tell.ogg");
      snd.play();

    }
  }

  return {
    // Function to send a desktop notification
    FireNotification : FireNotification
  }
}

})();
