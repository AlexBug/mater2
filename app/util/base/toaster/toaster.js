﻿/**
 * Author: Anthony Guo (anthony.guo@some.ox.ac.uk)
 *
 * Purpose: Duplicate the old angular toaster API, BUT use angular material's
 * toaster. Provides methods for updating.
 */

(function () {

angular.module('mater2.services').service('toaster',
  ['$mdToast', service]);
function service($mdToast) {
  function pop(type, message) {
    $mdToast.show({
      controller: 'ToastCtrl',
      templateUrl: 'app/util/base/toaster/toaster.html',
      hideDelay: 2000,
      position: 'top right',
      locals: { message: message, type: type},
    });
  }
  return {
    pop: pop,
  }
}

angular.module('mater2.services').controller('ToastCtrl',
  ['$scope', '$mdToast', 'message', 'type',
function ($scope, $mdToast, message, type) {
    $scope.closeToast = function () {
      $mdToast.hide();
    };
    $scope.message = message;
    $scope.toaster_style = {};
    switch (type) {
      case 'error':
        $scope.toaster_style['background-color'] = 'rgb(218, 78, 78)';
        break;
      case 'success':
        $scope.toaster_style['background-color'] = 'rgb(57, 142, 179)';
        break;
      default:
        break;
    }
}]);

})();
