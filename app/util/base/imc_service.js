/** Intermodule communicator service
 */
(function(){

var eventToCallbackMap = {};
angular.module('mater2.services').service('ImcService',
  [
function() {
  function addHandler(eventName, callback) {
    if (eventToCallbackMap[eventName] == null) {
      eventToCallbackMap[eventName] = [callback];
    } else {
      eventToCallbackMap[eventName].push(callback);
    }
  }

  return {
    addHandler: addHandler,
    fireEvent: function(eventName, data) {
      var callbacks = eventToCallbackMap[eventName];
      if (callbacks === null || callbacks === undefined) {
        console.log("No callbacks found for event " + eventName);
        return;
      }
      for (var i = 0; i != callbacks.length; ++i){
        var callback = callbacks[i];
        callback(data);
      }
    }
  }
}]);

})();
