/**
 * Stolen from:
 * https://gist.githubusercontent.com/lpsBetty/43be83926cab593ca86c/raw/focusMeDrct.js
 *
 * Sets focus to this element if the value of focus-me is true.
 * @example
 *  <a ng-click="addName=true">add name</a>
 *  <input ng-show="addName" type="text" ng-model="name" focus-me="{{addName}}" />
 */

angular.module("GlobalDirectives", []).directive('focusMe', ['$timeout',
function($timeout) {
  return {
    scope: { trigger: '@focusMe' },
    link: function(scope, element) {
      scope.$watch('trigger', function(value) {
        if(value === "true") {
          $timeout(function() {
            element[0].focus();
          });
        }
      });
    }
  };
}]);
