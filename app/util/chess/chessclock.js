(function(){
    angular.module('mater2.services')
    
    .directive('ngChessClock', ['$interval', function($interval){
        return {
            restrict: 'E',
            scope: {
                milliseconds: '=',
                highlight: '=',
                ticking: '='
            },
            template: '<div></div>',
            link: function ($scope, $element, $attrs) {
                var tick_interval;

                function FormatMilliseconds(duration){
                    var seconds = parseInt((duration/1000) % 60);
                    var minutes = parseInt((duration/(1000*60)) % 60);
                    seconds = (seconds < 10) ? "0" + seconds : seconds;
                    return (minutes + ":"  + seconds);
                }

                function HighlightClock() {
                    $element.attr("style", "color: white; background: black");
                }

                function UnhighlightClock() {
                    $element.attr("style", "color: black; background: white");
                }

                function StartTick() {
                    tick_interval = $interval(function() {
                        $scope.milliseconds -= 1000;
                    }, 1000);
                }

                function StopTick() {
                    if (tick_interval) {
                        $interval.cancel(tick_interval);
                        tick_interval = null;
                    }
                }

                function UpdateHighlight() {
                    if ($scope.highlight) {
                        HighlightClock();
                    } else {
                        UnhighlightClock();
                    }
                }

                function UpdateTicking() {
                    if ($scope.ticking) {
                        StartTick();
                    } else {
                        StopTick();
                    }
                }

                function DisplayClock() {
                    $element.html("&nbsp;" +
                                  FormatMilliseconds($scope.milliseconds) +
                                  "&nbsp;&nbsp;&nbsp;");
                    UpdateHighlight();
                }

                function WatchTickBool() {
                    $scope.$watch('ticking', UpdateTicking);
                }

                function WatchTheTime() {
                    $scope.$watch('milliseconds', DisplayClock);
                }

                function WatchHighlightAttr() {
                    $scope.$watch('highlight', UpdateHighlight);
                }

                // Display the clock.
                DisplayClock();

                // Attach watchers.
                WatchTheTime();
                WatchHighlightAttr();
                WatchTickBool();
            }
        }
    }]);
})();
