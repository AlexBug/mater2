/** Copied from mater.
 */

(function() {

angular.module('mater2.services').service('Timeseal',
function() {
  var timeseal_key = "Timestamp (FICS) v1.0 - programmed by Henrik Gram.";

  function _bswap(s, i, j) {
    console.assert(i < j);
    var t = s.substr(0, i) + s.charAt(j) + s.substr(i + 1, j - i - 1) +
      s.charAt(i) + s.substr(j + 1);
    return t;
  }

  function Encode(line) {
    var d = new Date();
    var ts = d.getTime() % 10000000;
    line = line + '\x18' + ts.toString(10) + '\x19';
    var len = line.length;
    while (len % 12 !== 0) {
      len++;
      line += '1';
    }
    for (var n = 0; n < len; n += 12) {
      line = _bswap(line, n + 0, n + 11);
      line = _bswap(line, n + 2, n + 9);
      line = _bswap(line, n + 4, n + 7);
    }
    console.assert(len === line.length);
    var newline = '';
    for (n = 0; n < len; n++) {
      var c = ((line.charCodeAt(n) | 0x80) ^ timeseal_key.charCodeAt(n % 50)) - 32;
      console.assert(c > 0);
      console.assert(c < 256);
      newline += String.fromCharCode(c);
    }
    newline = newline + String.fromCharCode(0x80) + '\x0a';

    // SockJS can't handle binary data, so base64-encode it
    newline =  window.btoa(newline);
    return newline;
  }

  PingReply = '\x02' + '9';

  return {
    // Encodes a string according to timeseal protocol.
    Encode: Encode,

    // Returns a string as the reply for a ping from the server.
    PingReply: PingReply,
  };
});

})();
