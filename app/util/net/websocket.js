/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 *
 * Simple adapter to connect to the server. Wrapper
 * around SockJS.
 *
 * It returns a function constructor because otherwise we will not be able
 * to instantiate more than one websocket
 */
(function(){

angular.module('mater2.services').service('Websocket',
function (socketFactory) {
  function Websocket(){
    this.socket = null;
  }

  function SendLine(msg) {
    this.socket.send(msg);
  }

  function Connect(url) {
    this.socket = socketFactory({url: url});
  }

  function Close() {
    this.socket.close();
  }

  function SetHandler(event_name, callback) {
    this.socket.setHandler(event_name, callback);
  }

  // Sends a line using the currently created websocket.
  Websocket.prototype.SendLine = SendLine;

  // Creates a websocket to a given URL.
  Websocket.prototype.Connect = Connect;

  // Implements the same event listeners as
  // SockJS: "open", "message", "close".
  Websocket.prototype.Close = Close;

  // Closes the websocket.
  Websocket.prototype.SetHandler = SetHandler;

  return Websocket;
});

})();
