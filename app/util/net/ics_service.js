/** Anthony Guo (anthony.guo@some.ox.ac.uk)
 *
 * Creates and interprets messages from the server.
 *
 * FIXME: Possible memory leak as eventhandlers are never
 * deregistered
 */
(function(){

// Interprets input received from the IcsAdapter.
// Each input corresponds to an event. When an event
// is triggered, all registered callbacks are invoked.
angular.module('mater2.services').service('IcsService',
  ['IcsAdapter',
function(IcsAdapter) {
  // The EDT stands for "External Descriptor Table",
  // analogous to the IDT and GDT data structures
  // in the operating system.
  //
  // Each element in the EDT has a regular expression
  // that helps describe an event. If a new message
  // matches the regular expression, then all the callbacks
  // are invoked -- in the order that they were added.
  //
  // Each callback in "callbacks" is expected to be:
  // {
  //   fn: <callback_fn>,
  //   once: <bool>,      // Deregister callback after invoking?
  // }
  //
  // Instead of a string, the data structure is a JSON obj to allow
  // future flexibility in adding new attributes.
  var EDT = {
    //-----------------------------------------------------------------
    // General events.

    // Server has sent some message.
    "all": {
      callbacks: [],
      regexp: /./,
    },

    // Server did not recognize command.
    "unrecognized command": {
      callbacks: [],
      parser: function(regexp_result) {
        var m = regexp_result;
        return {
          command: m[1]
        }
      },
      regexp: /(.*) Command not found.$/m,
    },

    //-----------------------------------------------------------------
    // Login events.

    // Server asks for a username.
    "username?": {
      callbacks: [],
      regexp: /login: /,
    },

    // Server asks for a password.
    "password?": {
      callbacks: [],
      regexp: /password: /,
    },

    // Server did not find username in database.
    "username not found!": {
      callbacks: [],
      regexp: /is not a registered name/,
    },

    // Username was not all alphabet characters.
    "username nonalpha!": {
      callbacks: [],
      regexp: /Sorry, names can only consist of/,
    },

    // Username/Password combo did not work.
    "password invalid!": {
      callbacks: [],
      regexp: /Invalid password!/,
    },

    // Server has created session for user.
    "session started": {
      callbacks: [],
      regexp: /\*\*\*\* Starting BICS session as (.*) \*\*\*\*/,
      parser: function(regexp_result) {
        var m = regexp_result;
        return {
          username: m[1]
        }
      }
    },

    //-----------------------------------------------------------------
    // Chat events.

    "received lobby message": {
      callbacks: [],
      regexp: /([A-Za-z]+)(\(.*\))?\(24\): (.*)/,
      parser: function(regexp_result) {
        var m = regexp_result;
        return {
          username: m[1],
          message: m[m.length-1]
        }
      }
    },

    "was told message": {
      callbacks: [],
      regexp: /tells you/
    },

    'told someone message': {
      callbacks: [],
      regexp: /\(told (.*)\)/
    },

    //-----------------------------------------------------------------
    // Match events.

    'match error message' : {
      callbacks: [],
      regexp: /No user named ".*" is logged in|Your opponent has no partner for bughouse|Your partner hasn't chosen you as his partner!|You can't specify time or inc above 999|Can't interpret .* in match command/
    },

    'match issued': {
      callbacks: [],
      regexp: /Issuing: /
    },

    //-----------------------------------------------------------------
    // Partnership events.

    'partner error message': {
      callbacks: [],
      regexp: /No user named ".*" is logged in|already has a partner./
    },
    'partnership issued' : {
      callbacks: [],
      regexp: /Making a partnership offer to/
    },

    // Was able to partner successfully
    'accept partnership successful': {
      callbacks :  [],
      regexp: /You agree to be ([A-Za-z]+)'s partner/,
      parser: function(regexp_result){
        var m = regexp_result;
        return {
          partner_name : m[1]
        }
      }
    },

    // Somebody accepted a partnership request
    'partnership request accepted': {
      callbacks: [],
      regexp: /([A-Za-z]+) agrees to be your partner/,
      parser: function(regexp_result){
        var m = regexp_result;
        return { 
          partner_name : m[1]
        }
      }
    },

    // Somebody declined a partnership request
    'partnership request declined': {
      callbacks: [],
      regexp: /([A-Za-z]+) declines your partnership request/,
      parser: function(regexp_result){
        var m = regexp_result;
        return { 
          partner_name : m[1]
        }
      }
    },

    //-----------------------------------------------------------------
    // Game events.

    // Server has ping'ed, asking for timeseal's timestamp.
    "pinged": {
      callbacks: [],
      regexp: /^\[G\]$/m
    },

    // Server sends info about a new game.
    "game started": {
      callbacks: [],
      parser: function(regexp_result) {
        var m = regexp_result;
        return {
          game_id: parseInt(m[1], 10),
          chess_variant: m[3],
          is_rated: parseInt(m[4], 10),
          white_player: {
            rating: m[12]
          },
          black_player: {
            rating: m[13]
          }
        };
      },
      regexp: /^<g1> (\d+) p=(\d+) t=(\w+) r=(\d+) u=(\d+),(\d+) it=(\d+),(\d+) i=(\d+),(\d+) pt=(\d+) rt=(\d+\w?),(\d+\w?) ts=(\d+),(\d+) m=(\d+) n=(\d+)/m
    },

    // Board has new state.
    "board updated": {
      callbacks: [],
      parser: function(regexp_result) {
        var m = regexp_result;
        return {
          position: m[1],
          turn: m[2],
          game_id: parseInt(m[9], 10),
          white_player: {
            name: m[10],
            time_ms: parseInt(m[17], 10)
          },
          black_player: {
            name: m[11],
            time_ms: parseInt(m[18], 10)
          },
          elapsed_time: parseInt(m[21], 10),
          clock_is_ticking: parseInt(m[24], 10),
          lag: parseInt(m[25], 10),
          relation: parseInt(m[12], 10),
          flip: parseInt(m[23], 10)
        }
      },
      regexp: /^<12> ((?:[-pPnNbBrRqQkK]{8} ){8})([WB]) (-?\d) ([01]) ([01]) ([01]) ([01]) (\d+) (\d+) (\w+) (\w+) (-?\d+) (\d+) (\d+) (\d+) (\d+) (-?\d+) (-?\d+) (\d+) ([^ ]+) \((\d+:\d+\.\d+)\) ([^ ]+) ([01]) ([01]) (\d+)/m
    },

    // Somebody just got a piece.
    "pieces-in-hand updated": {
      callbacks: [],
      parser: function (regexp_result) {
        var m = regexp_result;
        function setHand(hand) {
          if (hand === undefined) {
            return "";
          }
          return hand;
        }
        var white_hand = setHand(m[3]);
        var black_hand = setHand(m[5]);
        return {
          game_id: m[1],
          white_is_holding: white_hand,
          black_is_holding: black_hand
        }
      },
      regexp: /^<b1> game (\d+) white \[((\w+)|())\] black \[((\w+)|())\]/m
    },

    // Illegal move.
    "illegal move": {
      callbacks: [],
      parser: function(regexp_result) {
        var m = regexp_result;
        return {
          attempted_move: m[1]
        }
      },
      regexp: /^Illegal move \((.*)\)\.$/m
    },

    // Not currently the player's turn.
    "not my turn": {
      callbacks: [],
      parser: function(regexp_result) {
        return {};
      },
      regexp: /^It is not your move.$/m
    },

    // Game over.
    "game over": {
      callbacks: [],
      parser: function(regexp_result) {
        m = regexp_result;
        return {
          game_id: parseInt(m[1], 10),
          white_player_name: m[2],
          black_player_name: m[3],
          reason: m[4],
          result: m[5]
        };
      },
      regexp: /{Game (\d+) \(([A-Za-z]+) vs. ([A-Za-z]+)\) (.*?)} (1-0|0-1|1\/2-1\/2|\*)/m
    }
  };

  // Invoke "NotifyListeners" every time a message arrives.
  IcsAdapter.AddListener(NotifyListeners);

  var SendLine = IcsAdapter.SendLine;
  var NewConnection = IcsAdapter.NewConnection;

  // Immediately connect to IcsServer.
  NewConnection();

  function Do(event_name, args) {
    // TODO.
    console.assert(false);
    console.log("NYI");
  }

  function AddListener(event_name, callback, only_once) {
    console.assert(EDT[event_name] !== undefined);
    EDT[event_name].callbacks.push({
      fn: callback,
      once: only_once
    });
  }

  function NotifyListeners(response) {
    for (var i in EDT) {
      var regexp_result = EDT[i].regexp.exec(response.data);
      if (regexp_result) {
        for (var j in EDT[i].callbacks) {
          var callback = EDT[i].callbacks[j].fn;

          // Deregister callback if only meant to be called once.
          if (EDT[i].callbacks[j].once) {
            EDT[i].callbacks.splice(j, 1);
          }

          // Pass along any data regexp'd.
          if (EDT[i].parser) {
            response.parsed = EDT[i].parser(regexp_result);
          }

          // Invoke callback.
          callback(response);
        }
      }
    }
  }

  function Close() {
    IcsAdapter.Close();
  }

  return {
    // Sends a message to Ics. Assumes no newline
    // character in message.
    SendLine: SendLine,

    // Adds a listener for a particular Ics message.
    AddListener: AddListener,

    // Create a new connection to bughouseclub.com.
    // Throws away the old connection.
    NewConnection: NewConnection,

    // Close the connection.
    Close: Close,

    // There is no public API to change or close the offline adapter.
  }
}]);

})();
