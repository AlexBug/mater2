/**
 * Author: Alex Guo (chessnut@outlook.com).
 *
 * Purpose: Assortment of backends: chat, lobby, arena.
 *
 * FIXME: Possible memory leak as eventhandlers are never
 * deregistered
 */
(function() {

angular.module('mater2.services').service('BackendAdapters', ['Websocket',
function(Websocket) {
  // Each object in '_backends' is expected to be:
  // {
  //   "server_url": "...",
  //   "websocket": <Websocket obj>,
  //   // Each object in listeners is expected to be:
  //   // {
  //   //   callback: <callback_fn>,
  //   // }
  //   "last_response": null,
  //   "EDT": {
  //     "<event_name>": {
  //       listeners: [<callbacks>, ...],
  //     }
  //   }
  // }
  //
  // The EDT is a dict of possible events that the backend could fire.
  var _backends = {
    "arena": {
      "server_url": "http://en.bughouseclub.com:3005/bot",
      "EDT": {
        "wait": {},
        "suggestion": {},
      },
    },
    "chat": {
      "server_url": "http://en.bughouseclub.com:3006/bot",
      "EDT": {
        "new msg": {},
        "fetch returned": {},
        "error": {},
        "register success": {},
      },
    },
    "register": {
      "server_url": "http://en.bughouseclub.com:3007/bot",
      "EDT": {
        "register": {},
        "success": {},
        "error": {},
      },
    }
  };

  // Possible events to send to backends.
  var _IDT = {
              "seek": { backend_name: "arena" },
            "unseek": { backend_name: "arena" },

              "tell": { backend_name: "chat" },
     "chat_register": { backend_name: "chat" },
             "fetch": { backend_name: "chat" },

          "register": { backend_name: "register" },
  };

  InitBackends();
  StartConnections();

  function InitBackends() {
    for (var backend_name in _backends) {
      var backend = _backends[backend_name];
      backend.websocket = new Websocket();
      backend.listeners = [];
      var EDT = backend.EDT;
      for (var event_name in EDT) {
        EDT[event_name].listeners = [];
        EDT[event_name].last_response = null;
      }
    }
  }

  function StartConnections() {
    for (var backend_name in _backends) {
      var backend = _backends[backend_name];
      var server_url = backend.server_url;
      var websocket = backend.websocket;
      websocket.Connect(server_url);
      websocket.SetHandler('message', NotifyListeners);
      websocket.SetHandler('close', function(a){console.log(a);});
    }
  }

  var _start_time = Date.now();
  var _sufficient_start_time = false;
  function Do(event_name, data) {
    if (!_sufficient_start_time) {
      var one_second = 1000;
      if (Date.now() - _start_time > one_second) {
        _sufficient_start_time = true;
      } else {
        setTimeout(function(){ Do(event_name, data); }, one_second);
        return;
      }
    }
    var event = _IDT[event_name];
    if (event === undefined) {
      console.log(event_name + " not found.");
      return;
    }
    if (data === undefined) { data = {}; }
    data.type = event_name;
    // TODO: fix hack.
    if (data.type === "chat_register") {
      data.type = "register";
    }
    var backend = _backends[event.backend_name];
    backend.websocket.SendLine(JSON.stringify(data));
  }

  function NotifyListeners(response) {
    var data = JSON.parse(response.data);
    var backend_name = data.backend_name;
    var event_name = data.type;
    var event = _backends[backend_name].EDT[event_name];
    if (event === undefined) {
      console.error(event_name + " not found.");
      console.log(response);
      return;
    }
    event.last_response = response;
    event.listeners.map(function(listener) { listener.callback(data); });
  }

  function AddListenerInternal(callback, backend_name, event_name) {
    console.assert(backend_name !== undefined);
    if (event_name === undefined) { event_name = "*"; }
    var event = _backends[backend_name].EDT[event_name];
    var listeners = event.listeners;
    listeners.push({"callback": callback});
    var last_response = event.last_response;
    if (last_response) {
      listeners.map(function(listener) { listener.callback(last_response); });
    }
  }

  function AddListener(callback, backend_name, event_name) {
    if (backend_name === undefined) {
      for (var backend_name in _backends) {
        AddListenerInternal(callback, backend_name, event_name);
      }
    }
    AddListenerInternal(callback, backend_name, event_name);
  }

  return {
    // Fire events.
    Do: Do,

    // The callback will be invoked on any incoming message.
    // The callback will be passed a new message.
    AddListener: AddListener,
  };
}]);

})();
