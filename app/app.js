/**
 * Author: Anthony Guo (anthony.guo@some.ox.ac.uk).
 */

var app = angular.module('mater2', [
  'mater2.controllers',
  'mater2.services',
  'mater2.directives',
  'ngRoute',
  'ngMdIcons',
]);

angular.module('mater2.controllers', [
  'luegg.directives',
  'ng.chessboard',
  'mater2.services',
  'GlobalDirectives',
  'ngMaterial',
]);

angular.module('mater2.services', [
  'bd.sockjs',
  'ngCookies',
  'ngMaterial',
]);

angular.module('mater2.directives', []);

/**
 * Controls the entire app. Named "MaterImproved".
 */
(function(){

app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider

  // route for the home page
  .when('/', {
    controller: 'HomePgCtrl',
    templateUrl: 'app/pages/home/main.html',
  })

  // route for the dashboard
  .when('/game', { templateUrl: 'app/pages/game/main.html', })
}]);

angular.module('mater2.controllers').controller('MaterImproved',
  ['$scope', 'LoginService', 'IcsService', 'ProfileService',
   'LocalizationService', '$http', 'GameService', '$mdDialog', 'ArenaService',
   controller]);
function controller($scope, LoginService, IcsService, ProfileService,
                    LocalizationService, $http, GameService, $mdDialog,
                    ArenaService) {
  $scope.locale = LocalizationService.Locale;
  $scope.GetUsername = ProfileService.GetUsername;
  $scope.is_playing = false;
  $scope.RedirectToSignup = function() {
    window.location = 'http://bughouseclub.com/register/';
  }

  $http.get('app/util/locales/general.json')
  .success(function(general_names) {
    $scope.english = general_names.english;
    $scope.russian = general_names.russian;
  });

  $scope.Resign = function() { IcsService.SendLine('resign'); }
  $scope.Rematch = function() { IcsService.SendLine('rematch'); }

  $scope.OpenRegisterDialog = function(ev) {
    $mdDialog.show({
      controller: 'RegisterCtrl',
      templateUrl: 'app/register/main.html',
      parent: angular.element(document.body),
      targetEvent: ev,
    });
  }

  $scope.OpenLoginDialog = function(ev) {
    $mdDialog.show({
      controller: 'LoginCtrl',
      templateUrl: 'app/login/main.html',
      parent: angular.element(document.body),
      targetEvent: ev,
    });
  }

  function genRandomId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (var i = 0; i < 4; ++i) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  window.onbeforeunload = function(ev) {
    var id = genRandomId();
    var name = "Guest" + id;
    return "Wait...\n\n\n\n" + name +
      " is looking for a chess game.\n\n\n" +
      "Do you want to play a game with him?";
  };

  LoginService.LoginDefaultSettings();
  IcsService.AddListener("game started", StartGame);
  IcsService.AddListener("game over", EndGame);

  function StartGame() {
    window.location = '#/game';
    $scope.is_playing = true;
  }

  function EndGame() {
    $scope.is_playing = false;
  }
} 
})();
