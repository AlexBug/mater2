/*
 * Author: Alex Guo (chessnut@outlook.com).
 * Description: Controller for the Arena frontend.
 */

(function() {

angular.module('mater2.controllers').controller('HomePgCtrl',
  ['$scope', 'LobbyChatService', controller]);
function controller($scope, LobbyChatService) {
  function randIdxIn(start, end) {
    return Math.floor(Math.random() * (end - start)) + start;
  }
  $scope.num_players = randIdxIn(200, 210);
  $scope.num_games = randIdxIn(27, 30);
  $scope.feedback = "";

  $scope.send_feedback = function() {
    LobbyChatService.SendMsg({msg: $scope.feedback});
    var feedback_div = document.getElementById("feedback");
    feedback_div.innerHTML = "&nbsp;&nbsp;&nbsp;Thanks for your feedback!";
  }
}

})();
