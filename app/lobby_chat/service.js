/**
 * Service for chatting to people
 */

(function(){

angular.module('mater2.services').service('LobbyChatService',
  ['IcsService', 'BackendAdapters', 'ProfileService', 'ImcService',
   'DesktopNotifs',
function(IcsService, BackendAdapters, ProfileService, ImcService,
         DesktopNotifs) {
  var _lobby = {
    messages: []
  };
  var _username;

  function SendMsg(data){
    if (data.msg === "" || data.msg === undefined) {
      return;
    }
    var data = {
      who: _username,
      chatroom: "all",
      msg: data.msg,
    };
    BackendAdapters.Do("tell", data);
  }

  function SessionStarted() {
    _username = ProfileService.GetUsername();
    BackendAdapters.Do("chat_register", {"who": _username});
    var test1 = typeof(WebSocket);
    var test2 = "undefined";
    if (WebSocket !== undefined &&
        WebSocket.prototype !== undefined) {
      test2 = typeof(WebSocket.prototype.send);
    }
    SendMsg({msg: "WebSocket test: " + test1 + ", " + test2 });
  }

  IcsService.AddListener("session started", SessionStarted, true);

  return {
    SendMsg: SendMsg,
    lobby: _lobby,
  }
}]);

})();
