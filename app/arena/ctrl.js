/*
 * Author: Alex Guo (chessnut@outlook.com).
 * Description: Controller for the Arena frontend.
 */

(function() {

angular.module('mater2.controllers').controller('ArenaCtrl',
  ['$scope', 'ArenaService', 'LocalizationService', 'LobbyChatService',
   '$mdDialog', '$interval',
   controller]);
function controller($scope, ArenaService, LocalizationService,
                    LobbyChatService, $mdDialog, $interval) {
  $scope.locale = LocalizationService.Locale;

  $scope.start_bughouse_seek = function(ev) {
    ArenaService.FrontendSeek("bug", ev);
  }

  $scope.start_chess_seek = function(ev) {
    ArenaService.FrontendSeek("5 min", ev);
  }

  $scope.aggressive_seek = function(ev) {
    LobbyChatService.SendMsg({msg: "Accept."});
    ArenaService.Seek5Min();
  }

  function genRandomId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (var i = 0; i < 4; ++i) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  function genRandomSeeks() {
    var result = [];
    for (var i = 0; i < 4; ++i) {
      var id = genRandomId();
      var player_name = "Guest" + id;
      result.push({player_name: player_name});
    }
    return result;
  }
  $scope.chess_seeks = genRandomSeeks();
  $scope.partner_seeks = genRandomSeeks();

  $scope.offers = [
    {player_name: "Guest" + genRandomId()},
    {player_name: "Guest" + genRandomId()},
    {player_name: "Guest" + genRandomId()},
  ];

  var __counter__ = 0;
  function mutateSeeks() {
    __counter__ += 1;
    if (__counter__ % 2 == 0) {
      $scope.chess_seeks.shift();
      $scope.partner_seeks.shift();
    } else {
      $scope.chess_seeks.push({
        player_name: "Guest" + genRandomId()
      });
      $scope.partner_seeks.push({
        player_name: "Guest" + genRandomId()
      });
    }
  }

  var one_sec = 1000;
  var continuously_mutate_seeks = $interval(mutateSeeks, one_sec);
}

})();
