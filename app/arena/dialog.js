/**
 * Author: Alex Guo (chessnut@outlook.com).
 * Purpose: Controls the Arena waiting dialog.
 */
(function() {

angular.module('mater2.controllers').controller('ArenaWaitingCtrl',
  ['$scope', 'ArenaService', '$mdDialog', controller])
function controller($scope, ArenaService, $mdDialog) {
  var listen_only_once = true;
  ArenaService.AddListener("exited_arena", hideDialog, listen_only_once);

  $scope.Cancel = function() {
    ArenaService.UnseekAll();
  }

  function hideDialog() {
    $mdDialog.hide({ status: 'success' });
  }
}

})();
