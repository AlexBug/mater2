/**
 * Service for chatting to people
 */

(function() {

angular.module('mater2.services').service('ArenaService',
['BackendAdapters', 'ProfileService', '$interval', 'IcsService', '$timeout',
 '$mdDialog', 'LobbyChatService',
function(BackendAdapters, ProfileService, $interval, IcsService, $timeout,
         $mdDialog, LobbyChatService) {
  var three_secs = 3 * 1000;
  var _partner = undefined;
  var _poll_chess_pool = undefined;
  var _async = undefined;

  var EDT = {
    exited_arena: { callbacks: [], },
  };

  function GotSuggestion(data) {
    if (data.pool === "partner") {
      _partner = data.suggestion;
      IcsService.SendLine('partner ' + _partner);
    }
    if (data.pool === "bug") {
      var first_guy = 0;
      var opp = data.suggestion[first_guy];
      var cmd = ["match", opp, "bug", "3 0"];
      IcsService.SendLine(cmd.join(' '));
    }
    if (data.pool === "5-min chess") {
      var opp = data.suggestion;
      var cmd = ["match", opp, "3 0"];
      IcsService.SendLine(cmd.join(' '));
    }
  }

  function PartnershipSuccess() {
    IcsService.SendLine("pfollow " + _partner);
    BackendAdapters.Do("unseek", {pool: "partner"});
    SeekBug(_partner);
  }

  function UnseekAll() {
    $timeout.cancel(_async);
    $interval.cancel(_poll_chess_pool);
    _poll_chess_pool = undefined;
    BackendAdapters.Do("unseek", {pool: "5-min chess"});
    BackendAdapters.Do("unseek", {pool: "partner"});
    BackendAdapters.Do("unseek", {pool: "bug"});
    NotifyListeners("exited_arena");
  }

  function SeekPartner() {
    BackendAdapters.Do("seek", {who: ProfileService.GetUsername(),
                                pool: "partner"});
  }

  function SeekBug(partner_name) {
    BackendAdapters.Do("seek",
                       {who: [ProfileService.GetUsername(), partner_name],
                        pool: "bug"});
  }

  function Seek5Min() {
    if (_poll_chess_pool === undefined) {
      _poll_chess_pool = $interval(Seek5Min, three_secs);
    }
    BackendAdapters.Do("seek", {who: ProfileService.GetUsername(),
                                pool: "5-min chess"});
  }

  function Async(pool_name, time_to_wait) {
    var pool_fn;
    if (pool_name === "5 min") { pool_fn = Seek5Min; }
    if (pool_name === "bug")  { pool_fn = SeekPartner; }
    _async = $timeout(pool_fn, time_to_wait);
  }

  function AddListener(event_name, callback, listen_only_once) {
    console.assert(EDT[event_name] !== undefined);
    EDT[event_name].callbacks.push({
      fn: callback,
      once: listen_only_once,
    });
  }

  function NotifyListeners(event_name, args) {
    console.assert(EDT[event_name] !== undefined);
    for (var i in EDT[event_name].callbacks) {
      var callback = EDT[event_name].callbacks[i];
      if (callback.once) { EDT[event_name].callbacks.splice(i, 1); }
      callback.fn(args);
    }
  }

  function FrontendSeek(pool_name, ev) {
    LobbyChatService.SendMsg({msg: "Seek " + pool_name + "."});
    $mdDialog.show({
      controller: 'ArenaWaitingCtrl',
      templateUrl: 'app/arena/dialog.html',
      parent: angular.element(document.body),
      targetEvent: ev,
    });
    var three_secs = 3000;
    var wait_time = three_secs;
    Async(pool_name, wait_time);
  }

  BackendAdapters.AddListener(GotSuggestion, "arena", "suggestion");
  IcsService.AddListener("partnership request accepted", PartnershipSuccess);
  IcsService.AddListener("game started", UnseekAll);

  return { SeekPartner: SeekPartner,
           Seek5Min: Seek5Min,
           UnseekAll: UnseekAll,
           Async: Async,
           FrontendSeek: FrontendSeek,

           AddListener: AddListener, };
}]);

})();
