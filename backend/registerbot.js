/*
 * Copyright 2015 (c) BughouseClub.com
 *
 * Author: Alex Guo (chessnut@outlook.com).
 *
 * Purpose: backend server that sets up arena for players. Does not actually
 *          do auto-partner or auto-match, but suggests to the users which
 *          people or teams they can play.
 */

var http = require('http');
var net = require('net');
var sockjs = require('sockjs');
var fs = require('fs');
var crypt = require('crypt3');

// TODO: put magic constants in config file.
var RegisterBotPort = 3007;
var RegisterBotTestPort = 5007;

var ports = {
  production: RegisterBotPort,
  test: RegisterBotTestPort,
};

// Figure out a 'register_dir' to use.
// TODO: do this in a cleaner way, please.
var register_dir = "/var/bics/players";
if (process.argv[2] !== undefined) {
  register_dir = process.argv[2];
}
console.log("register_dir: " + register_dir);

var event_handlers = {
  //---------------------------------------------------------------------------
  // Valid commands.
  'register': onRegister,
};

function checkFields(msg, fields) {
  for (var i = 0; i < fields.length; ++i) {
    if (msg[fields[i]] === undefined) {
      return false;
    }
  }
  return true;
}

function genCheckFieldsError(fields) {
  return {
    type: "error",
    msg: "Need fields: " + JSON.stringify(fields),
  };
}

function checkUsername(username) {
  function alphabetic(str) { return /^[a-zA-Z]+$/.test(str) }
  if (!alphabetic(username)) { return false; }
  if (username.length < 3) { return false; }
  if (username.length > 20) { return false; }
  return true;
  // Checking whether user exists happens when username is being written to.
}

function encryptPassword(password) {
  var salt = "$1$aa";
  return crypt(password, salt);
}

function calcUserFile(username) {
  var fingerprint = username.toLowerCase();
  var bucket = fingerprint[0];
  var userfile = register_dir + "/" + bucket + "/" + fingerprint;
  return userfile;
}

function templateUserFileData(msg, callback) {
  var blank_fullname = "";
  fs.readFile("newuser.template", "utf8", function(err, data) {
    if (err) {
      console.error("Insane! 'newuser.template' doesn't exist!");
      callback("'newuser.template' didn't exist.");
      return;
    }
    data = data.replace("%u", msg.username);
    data = data.replace("%p", encryptPassword(msg.password));
    data = data.replace("%e", msg.email);
    data = data.replace("%f", blank_fullname);
    callback(undefined, data);
  });
}

function registerUser(msg, conn) {
  var userfile = calcUserFile(msg.username);
  fs.stat(userfile, function(err) {
    if (!err) {
      conn.writeJSON({type: "error", msg: "User already exists."});
      return;
    }
    templateUserFileData(msg, function(err, data) {
      fs.writeFile(userfile, data, function(err) {
        if (err) {
          conn.writeJSON({type: "error", msg: "Write failed."});
        } else {
          conn.writeJSON({type: "success", msg: "Registered."});
          var time = new Date().toISOString().
            replace(/T/, ' ').
            replace(/\..+/, '');
          // TODO: log the IP address as well.
          fs.appendFile("/var/www/gui2/secret_log.txt",
                        time + "\t" + msg.username + "\t" + msg.email + "\n");
        }
      });
    });
  });
}

function onRegister(msg, conn) {
  var required_fields = ["username", "password", "email"];
  if (!checkFields(msg, required_fields)) {
    conn.writeJSON(genCheckFieldsError(required_fields)); return;
  }
  if (!checkUsername(msg.username)) {
    conn.writeJSON({type: "error", msg: "Username invalid."}); return;
  }
  registerUser(msg, conn);
}

//-----------------------------------------------------------------------------
// Thin layer behind frontend. Hooks into 'event_handlers'.
// TODO: These functions should be put into a module that both the arena
// and the chat backends could reuse.
//-----------------------------------------------------------------------------

function onNewMsg(data, conn) {
  try {
    var msg = JSON.parse(data);
  } catch (err) {
    conn.writeJSON({type: 'error', msg: 'Could not JSON parse msg'}); return;
  }

  if (msg.type === undefined) {
    conn.writeJSON({type: 'error', msg: 'We need "type".'}); return;
  }

  var event_handler = event_handlers[msg.type];
  if (event_handler === undefined) {
    conn.writeJSON({type: 'error', msg: 'Msg type not supported.'}); return;
  }
  event_handler(msg, conn);
}

function _onLostConnection(conn) {
  // Register bot does not need to keep any state.
  return;
}

//-----------------------------------------------------------------------------
// Frontend. Starts services on a couple ports.
//-----------------------------------------------------------------------------

function writeJSON(msg) {
  msg.backend_name = "register";
  this.write(JSON.stringify(msg));
}

function startTestService(port_num) {
  // A telnet server.
  if (port_num === undefined) {
    return;
  }
  net.createServer(function(socket) {
    socket.writeJSON = writeJSON;
    socket.on('data', function(data) { onNewMsg(data, socket); });
    socket.on('close', function() { _onLostConnection(socket); });
    socket.on('error', function() { _onLostConnection(socket); });
  })
  .listen(port_num);
}

function startProductionService(port_num) {
  // A websocket server.
  if (port_num === undefined) {
    return;
  }
  var bot = sockjs.createServer();
  bot.on('connection', function(conn){
    conn.writeJSON = writeJSON;
    conn.on('data', function(data) { onNewMsg(data, conn); });
    conn.on('close', function() { _onLostConnection(conn); });
    conn.on('error', function() { _onLostConnection(conn); });
  });
  var server = http.createServer();
  bot.installHandlers(server, {prefix: '/bot'});
  server.listen(port_num, '0.0.0.0');
}

startProductionService(ports.production);
startTestService(ports.test);

console.log('Bot started. Listening on ports: ');
console.log(ports);
