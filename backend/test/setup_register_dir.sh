#!/bin/sh

# Clean register_dir.
rm -r register_dir

# Setup register_dir.
mkdir -p register_dir
for letter in {a..z}
do
  mkdir -p register_dir/$letter
done
