#!/usr/bin/python

import json
import sys
import telnetlib
import time

# TODO: put magic constant '5005' into a separate file.
tn = telnetlib.Telnet("localhost", "5005")

def send_sjeng_seek():
  msg = { "type": "seek", "pool": "partner", "who": "sjeng"}
  tn.write(json.dumps(msg))

def send_sjengb_seek():
  msg = { "type": "seek", "pool": "5-min chess", "who": "sjengb"}
  tn.write(json.dumps(msg))

def send_sjengc_seek():
  msg = { "type": "seek", "pool": "5-min chess", "who": "sjengc"}
  tn.write(json.dumps(msg))

def send_sjengd_seek():
  msg = { "type": "seek", "pool": "5-min chess", "who": "sjengd"}
  tn.write(json.dumps(msg))

def send_tjchess_seek():
  msg = { "type": "seek", "pool": "bug", "who": ["TJChessA", "TJChessB"]}
  tn.write(json.dumps(msg))

def sleep_a_little():
  tiny_amount_of_time = 0.2
  time.sleep(tiny_amount_of_time)

sleep_a_little()
send_sjeng_seek()

sleep_a_little()
send_sjengb_seek()

sleep_a_little()
send_sjengc_seek()

sleep_a_little()
send_sjengd_seek()

sleep_a_little()
send_tjchess_seek()

# Spin wheels around for a wheel. Don't die.
while True:
  one_year = 60 * 60 * 24 * 365
  time.sleep(one_year)
  continue
