/*
 * Copyright 2015 (c) BughouseClub.com
 *
 * Author: Alex Guo (chessnut@outlook.com).
 *
 * Purpose: backend server that sets up arena for players. Does not actually
 *          do auto-partner or auto-match, but suggests to the users which
 *          people or teams they can play.
 */

var http = require('http');
var net = require('net');
var sockjs = require('sockjs');
var fs = require("fs");

// TODO: put magic constants in config file.
var ChatBotPort = 3006;
var ChatBotTestPort = 5006;

var ports = {
  production: ChatBotPort,
  test: ChatBotTestPort,
};

var chatrooms = {
  all: {
    // Each obj in 'people' is: {
    //   'who': <string>
    //   'conn': <connection Obj>
    // };
    people: [],

    // Each obj in 'messages is: {
    //   'who': <string>
    //   'time': <int, in Unix timestamp, in milliseconds>
    //   'msg': <string>
    // };
    messages: [],
  },
};

var event_handlers = {
  //---------------------------------------------------------------------------
  // Valid commands.
  'tell': onTell,
  'register': onRegister,
  'fetch': onFetch,

  //---------------------------------------------------------------------------
  // Hook for 'lost_connection'.
  'lost_connection': onLostConnection,

  //---------------------------------------------------------------------------
  // Debug commands.
  'who': onWho,
};

function checkFields(msg, fields) {
  for (var i = 0; i < fields.length; ++i) {
    if (msg[fields[i]] === undefined) {
      return false;
    }
  }
  return true;
}

function genCheckFieldsError(fields) {
  return {
    type: "error",
    msg: "Need fields: " + JSON.stringify(fields),
  };
}

/* Purpose: Returns the last "num" most recent messages before "time".
 *          If "time" is undefined, then Date.now() is used instead.
 *
 * NOTE: teehee, I'm lying! For simplicity's sake, we are just ignoring the
 * "time" parameter for now.
 */
function onFetch(msg, conn) {
  var required_fields = ["chatroom", "num"];
  if (!checkFields(msg, required_fields)) {
    conn.writeJSON(genCheckFieldsError(required_fields)); return;
  }
  var chatroom = chatrooms[msg.chatroom];
  if (chatroom === undefined) {
    conn.writeJSON({type: "error", msg: "Chatroom does not exist."}); return;
  }
  conn.writeJSON({
    type: "fetch returned",
    messages: chatroom.messages.slice(0, msg.num),
  });
}

function stupidLog(msg) {
  fs.appendFile("/root/chat_log.txt", JSON.stringify(msg) + "\n");
}

function onTell(msg, conn) {
  var required_fields = ["chatroom", "who", "msg"];
  if (!checkFields(msg, required_fields)) {
    conn.writeJSON(genCheckFieldsError(required_fields)); return;
  }
  var chatroom = chatrooms[msg.chatroom];
  if (chatroom === undefined) {
    conn.writeJSON({type: "error", msg: "Chatroom does not exist."}); return;
  }
  var new_msg = {
    time: Date.now(),
    who: msg.who,
    msg: msg.msg,
  }
  new_msg.ip = conn.remoteAddress;
  stupidLog(new_msg);
  for (var i = 0; i < chatroom.people.length; ++i) {
    chatroom.people[i].conn.writeJSON({ type: "new msg", msg: new_msg });
  }
  chatroom.messages.unshift(new_msg);
  // XXX: teehee, I hardcoded 100. Are you going to complain?
  if (chatroom.messages.length > 100) { chatroom.messages.pop(); }
}

function onWho(msg, conn) {
  conn.writeJSON(
    chatrooms.all.people.map(function(person) { return person.who; })
  );
}

function onRegister(msg, conn) {
  var required_fields = ["who"];
  if (!checkFields(msg, required_fields)) {
    conn.writeJSON(genCheckFieldsError(required_fields)); return;
  }
  chatrooms.all.people.push({conn: conn, who: msg.who});
  conn.writeJSON({type: "register success", msg: "Registered in chat."});
  stupidLog({connected: true,
             ip: conn.remoteAddress,
             who: msg.who,
             time: Date.now(), });
}

function onLostConnection(conn) {
  for (var i in chatrooms) {
    for (var ii = 0; ii < chatrooms[i].people.length; ++ii) {
      if (conn === chatrooms[i].people[ii].conn) {
        chatrooms[i].people.splice(ii, 1);
        // Readjust the iterator now that the array has been shifted.
        --ii;
      }
    }
  }
}

//-----------------------------------------------------------------------------
// Thin layer behind frontend. Hooks into 'event_handlers'.
// TODO: These functions should be put into a module that both the arena
// and the chat backends could reuse.
//-----------------------------------------------------------------------------

function onNewMsg(data, conn) {
  try {
    var msg = JSON.parse(data);
  } catch (err) {
    conn.writeJSON({type: 'error', msg: 'Could not JSON parse msg'}); return;
  }

  if (msg.type === undefined) {
    conn.writeJSON({type: 'error', msg: 'We need "type".'}); return;
  }

  var event_handler = event_handlers[msg.type];
  if (event_handler === undefined) {
    conn.writeJSON({type: 'error', msg: 'Msg type not supported.'}); return;
  }
  event_handler(msg, conn);
}

function _onLostConnection(conn) {
  event_handlers["lost_connection"](conn);
}

//-----------------------------------------------------------------------------
// Frontend. Starts services on a couple ports.
//-----------------------------------------------------------------------------

function writeJSON(msg) {
  msg.backend_name = "chat";
  this.write(JSON.stringify(msg));
}

function startTestService(port_num) {
  // A telnet server.
  if (port_num === undefined) { return; }
  net.createServer(function(socket) {
    socket.writeJSON = writeJSON;
    socket.on('data', function(data) { onNewMsg(data, socket); });
    socket.on('close', function() { _onLostConnection(socket); });
    socket.on('error', function() { _onLostConnection(socket); });
  })
  .listen(port_num);
}

function startProductionService(port_num) {
  // A websocket server.
  if (port_num === undefined) { return; }
  var bot = sockjs.createServer();
  bot.on('connection', function(conn){
    conn.writeJSON = writeJSON;
    conn.on('data', function(data) { onNewMsg(data, conn); });
    conn.on('close', function() { _onLostConnection(conn); });
    conn.on('error', function() { _onLostConnection(conn); });
  });
  var server = http.createServer();
  bot.installHandlers(server, {prefix: '/bot'});
  server.listen(port_num, '0.0.0.0');
}

startProductionService(ports.production);
startTestService(ports.test);

console.log('Bot started. Listening on ports: ');
console.log(ports);
