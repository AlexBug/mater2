/*
 * Copyright 2015 (c) BughouseClub.com
 *
 * Author: Alex Guo (chessnut@outlook.com).
 *
 * Purpose: backend server that sets up arena for players. Does not actually
 *          do auto-partner or auto-match, but suggests to the users which
 *          people or teams they can play.
 */

// Returns a random number in the range [0, 'limit').
// TODO: put utility function in its own file.
function getRandomIdx(limit) {
  var arbitrarily_large_num = 1000000;
  var random_num = (Math.random() * arbitrarily_large_num) % limit;
  return Math.floor(random_num);
}

var http = require('http');
var net = require('net');
var sockjs = require('sockjs');

// TODO: put magic constants in config file.
var ArenaBotPort = 3005;
var ArenaBotTestPort = 5005;

var ports = {
  production: ArenaBotPort,
  test: ArenaBotTestPort,
};

var arena = {
  "5-min chess": {
    // Each obj in 'elems' has: {
    //   'who': <some JSON obj>,
    //   'conn': <connection Obj>,
    //   'signature': <string>,
    // };
    elems: [],
    sign: function(elem) { return elem.who; }
  },
  partner: {
    elems: [],
    sign: function(elem) { return elem.who; }
  },
  bug: {
    elems: [],
    sign: function(elem) { return elem.who.sort().join(); }
  }
};

var event_handlers = {
  seek:            onSeek,
  who:             onWho,
  unseek:          onUnseek,
  lost_connection: onLostConnection,
};

function writePoolError(msg, conn) {
  conn.writeJSON({type: 'error', msg: 'Pool does not exist.', pool: msg.pool});
}

function removeConn(pool, conn) {
  var result = false;
  for (var i = 0; i < pool.elems.length; ++i) {
    if (conn === pool.elems[i].conn) {
      pool.elems.splice(i, 1);
      --i; // Now that the array has shortened, adjust the index.
      result = true; // We actually removed someone.
    }
  }
  return result;
}

function insertElem(pool, msg, conn) {
  var signature = pool.sign(msg);
  for (var i = 0; i < pool.elems.length; ++i) {
    if (signature === pool.elems[i].signature) {
      return pool.elems[i];
    }
  }
  var new_elem = {
    who:       msg.who,
    signature: signature,
    conn:      conn,
  };
  pool.elems.push(new_elem);
  return new_elem;
}

function onWho(msg, conn) {
  var pool = arena[msg.pool];
  if (pool == undefined) { writePoolError(msg, conn); return; }
  conn.writeJSON({
    elems: pool.elems.map(function(elem) { return elem.who; }),
    pool: msg.pool,
  });
}

function onSeek(msg, conn) {
  var pool = arena[msg.pool];
  if (pool == undefined) { writePoolError(msg, conn); return; }
  if (msg.who === undefined) { conn.writeJSON({type: 'error',
                                               msg: 'Need "who" field.'}); }
  var elem = insertElem(pool, msg, conn);
  if (pool.elems.length <= 1) { conn.writeJSON({type: 'wait',
                                                msg: 'Please wait'});
                                return; }
  // Calculate a suggestion.
  var suggestion;
  do {
    var random_idx = getRandomIdx(pool.elems.length);
    suggestion = pool.elems[random_idx];
  } while (elem.signature === suggestion.signature);

  // Write out suggestions.
  var ret_msg = {type: 'suggestion', suggestion: msg.who, pool: msg.pool};
  suggestion.conn.writeJSON(ret_msg);
  ret_msg.suggestion = suggestion.who;
  conn.writeJSON(ret_msg);
}

function onUnseek(msg, conn) {
  var pool = arena[msg.pool];
  if (pool == undefined) { writePoolError(msg, conn); return; }
  if (removeConn(pool, conn)) {
    conn.writeJSON({type: 'success',
                    msg: 'Removed user successfully.',
                    pool: msg.pool});
    return;
  }
  conn.writeJSON({type: 'error',
                  msg: 'User was not found in pool.',
                  pool: msg.pool});
}

function onLostConnection(conn) {
  for (var pool_name in arena) { removeConn(arena[pool_name], conn); }
}

//-----------------------------------------------------------------------------
// Thin layer behind frontend. Hooks into 'event_handlers'.
// TODO: These functions should be put into a module that both the arena
// and the chat backends could reuse.
//-----------------------------------------------------------------------------

function onNewMsg(data, conn) {
  try {
    var msg = JSON.parse(data);
  } catch (err) {
    conn.writeJSON({type: 'error', msg: 'Could not JSON parse msg'}); return;
  }

  if (msg.type === undefined) {
    conn.writeJSON({type: 'error', msg: 'We need "type".'}); return;
  }

  var event_handler = event_handlers[msg.type];
  if (event_handler === undefined) {
    conn.writeJSON({type: 'error', msg: 'Msg type not supported.'}); return;
  }
  event_handler(msg, conn);
}

function _onLostConnection(conn) { event_handlers["lost_connection"](conn); }

//-----------------------------------------------------------------------------
// Frontend. Starts services on a couple ports.
//-----------------------------------------------------------------------------

function writeJSON(msg) {
  msg.backend_name = "arena";
  this.write(JSON.stringify(msg));
}

function startTestService(port_num) {
  // A telnet server.
  if (port_num === undefined) { return; }
  net.createServer(function(socket) {
    socket.writeJSON = writeJSON;
    socket.on('data', function(data) { onNewMsg(data, socket); });
    socket.on('close', function() { _onLostConnection(socket); });
    socket.on('error', function() { _onLostConnection(socket); });
  })
  .listen(ports.test);
}

function startProductionService(port_num) {
  // A websocket server.
  if (port_num === undefined) { return; }
  var bot = sockjs.createServer();
  bot.on('connection', function(conn){
    conn.writeJSON = writeJSON;
    conn.on('data', function(data) { onNewMsg(data, conn); });
    conn.on('close', function() { _onLostConnection(conn); });
    conn.on('error', function() { _onLostConnection(conn); });
  });
  var server = http.createServer();
  bot.installHandlers(server, {prefix: '/bot'});
  server.listen(ports.production, '0.0.0.0');
}

startProductionService(ports.production);
startTestService(ports.test);

console.log('Bot started. Listening on ports: ');
console.log(ports);
