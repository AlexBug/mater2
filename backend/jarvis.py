#!/usr/bin/python

import json
import sys
import telnetlib
import time

def main():
  # TODO: put magic constants into a separate file.
  server = "bughouseclub.com"
  port = 5000
  chat_port = 5006

  tn = telnetlib.Telnet(server, port)
  login(tn, username="jarvis", password="ficsadmin")

  chat_tn = telnetlib.Telnet(server, chat_port)
  while True:
    new_person = parse(tn.read_until("$"))
    chat_tn.write(json.dumps({
      "type": "tell",
      "msg": "Welcome %s to the Bughouse Club!" % new_person,
      "chatroom": "all",
      "who": "jarvis",
    }))

#------------------------------------------------------------------------------
# Helper functions.
#------------------------------------------------------------------------------

def login(tn, username=None, password=None):
  print tn.read_until(b"login: ", 2)
  tn.write(username+"\n")

  print tn.read_until("password: ", 2)
  tn.write(password+"\n\n")

"""
FIXME: someone can just "tell ROBOadmin ^user$" and ROBOadmin will listen!
We need to fix this.
"""
def parse(new_person):
  beg = new_person.rfind('^')
  end = new_person.rfind('$')
  return new_person[beg+1:end]

if __name__ == "__main__":
  main()
