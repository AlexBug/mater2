'''
Generates language json files
'''
import json
import requests
import urllib
import argparse

# Because google now costs MONEY!!??? we'll use msft

ENGLISH_SERIALIZED = open('../../app/util/locales/english.json', 'r').read()
ENGLISH_JSON = json.loads(ENGLISH_SERIALIZED)

CLIENT_ID = 'BughouseClubGenerator'
CLIENT_SECRET = '6w8HIZEnHjuAf68e8NAEVL9KeF/XkLj9QBCnWLllKw4='

def translate_phrase(phrase, output_lang_code):
    'Returns the translation of a word to an output language'
    args = {
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'scope': 'http://api.microsofttranslator.com',
        'grant_type': 'client_credentials'
    }
    oauth_url = 'https://datamarket.accesscontrol.windows.net/v2/OAuth2-13'
    oauth_junk = json.loads(requests.post(oauth_url,data=urllib.urlencode(args)).content)
    translation_args = {
        'text': phrase,
        'to': output_lang_code,
        'from': 'en'
    }
    headers = {'Authorization': 'Bearer '+oauth_junk['access_token']}
    translation_url = 'http://api.microsofttranslator.com/V2/Ajax.svc/Translate?'
    translation_result = requests.get(translation_url+urllib.urlencode(translation_args),headers=headers)

    # Strip the beginning quote and ending quote
    return translation_result.content.replace('"', '')

def main():
    'main'
    out = {}
    parser = argparse.ArgumentParser()
    parser.add_argument('lang_code')
    parser.add_argument('output_name')
    args = parser.parse_args()
    lang_code = args.lang_code
    output_name = args.output_name
    if not lang_code:
        print 'no language code supplied - exiting'
        return
    elif not output_name:
        print 'no output name supplied - exiting'
        return
    for phrase_name in ENGLISH_JSON.keys():
        phrase = ENGLISH_JSON[phrase_name]
        print phrase
        translated_phrase = translate_phrase(phrase, lang_code)
        out[phrase_name] = translated_phrase
        print phrase_name, translated_phrase
    with open('../../app/util/locales/' + output_name, 'w') as out_fopen:
        out_fopen.write(json.dumps(out, sort_keys=True,
                        indent=4, separators=(',', ': ')))
        out_fopen.flush()

if __name__ == '__main__':
    main()
